defmodule SpaceZRss.Repo.Migrations.CreateFeedEvents do
  use Ecto.Migration

  def change do
    create table(:feed_events) do
      add :feed, :string, null: false
      add :event_id, :string, null: false

      timestamps()
    end

    unique_index(:feed_events, [:feed, :event_id])
  end
end
