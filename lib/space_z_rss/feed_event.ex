defmodule SpaceZRss.FeedEvent do
  use Ecto.Schema

  schema "feed_events" do
    field :feed, :string
    field :event_id, :string

    timestamps()
  end

  def changeset(event, params \\ %{}) do
    event
    |> Ecto.Changeset.cast(params, [:feed, :event_id])
    |> Ecto.Changeset.validate_required([:feed, :event_id])
    |> Ecto.Changeset.unique_constraint([:feed, :event_id])
  end
end
