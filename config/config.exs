import Config

config :space_z_rss,
  ecto_repos: [SpaceZRss.Repo]

config :logger, :console,
  format: "[$time][$level]$metadata: $message\n",
  metadata: [:shard, :guild, :channel, :feedid]
