defmodule SpaceZRss.Watcher do
  use DynamicSupervisor
  require Logger

  def start_link(ia) do
    DynamicSupervisor.start_link(__MODULE__, ia, name: __MODULE__)
  end

  def init(_ia) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def add_feed(feed_info) do
    Logger.info("registering feed: #{inspect feed_info}")
    DynamicSupervisor.start_child(__MODULE__, {SpaceZRss.Worker, feed_info})
    |> IO.inspect()
  end
end
