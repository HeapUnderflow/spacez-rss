import Config
import AloyUtils.Env.ConfigHelpers, only: [get_env: 1, get_env: 2, get_env: 3]

AloyUtils.Env.dotenv_dev(Config.config_env() == :dev)

pg_socket = get_env("DB_SOCKET", nil)
if pg_socket != nil do
  config :space_z_rss, SpaceZRss.Repo, socket_dir: pg_socket
else
  config :space_z_rss, SpaceZRss.Repo, hostname: get_env("DB_HOST", "localhost")
end

config :space_z_rss, SpaceZRss.Repo,
  username: get_env("DB_USER", "spacez_rss"),
  password: get_env("DB_PASS", "spacez_rss"),
  database: get_env("DB_NAME", "spacez_rss_#{Config.config_env()}"),
  pool_size: get_env("DB_POOL_SIZE", 10, :integer),
  timeout: get_env("DB_TIMEOUT", 60_000, :integer)

config :nostrum,
  token: get_env("DISCORD_TOKEN"),
  gateway_intents: [:guilds]

config :space_z_rss,
       feed_info: "./feeds.toml"

case Config.config_env() do
  :prod ->
    config :logger, level: :warn

  :dev ->
    config :space_z_rss, Pxbox.Repo, show_sensitive_data_on_connection_error: true
end
