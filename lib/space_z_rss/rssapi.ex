defmodule SpaceZRss.RssApi do
  @moduledoc """
  Api Functions using finch to request a new event from the rss
  """

  alias SpaceZRss.Repo
  alias SpaceZRss.FeedEvent
  import Ecto.Query

  require Logger

  defmodule RssFeed do
    defstruct [:name, :feed]
  end

  @spec fetch_feed(String.t(), String.t()) :: RssFeed
  def fetch_feed(name, uri) do
    response = Finch.build(:get, uri)
    |> Finch.request!(SpaceZRss.Finch)

    %RssFeed { name: name, feed: FeederEx.parse!(response.body) }
  end

  @spec filter_feed(RssFeed) :: RssFeed
  def filter_feed(rssfeed) do
    ne = Enum.filter(rssfeed.feed.entries, fn e ->
      Repo.one(
        from p in SpaceZRss.FeedEvent,
        where: p.feed == ^rssfeed.name and p.event_id == ^e.id
      ) == nil
    end)

    Map.update!(rssfeed, :feed, fn fi -> %FeederEx.Feed{ fi | entries: ne } end)
  end


  @spec update_feed(RssFeed) :: {:ok, [FeedEvent]} | {:error, Ecto.Multi.name(), any(), %{required(Ecto.Multi.name()) => any()}}
  def update_feed(rssfeed) do
    case do_update_feed(rssfeed) do
      {:ok, c} -> {:ok, Map.values(c)}
      x -> x
    end
  end

  defp do_update_feed(rssfeed) do
    case rssfeed.feed.entries do
      [] ->
        Logger.debug("no entries, skipping insert")
        {:ok, %{}}
      entries ->
        m = Enum.reduce(
          entries,
          Ecto.Multi.new(),
          fn v, acc ->
            Ecto.Multi.insert(acc,
              {:feed_event, v.id},
              FeedEvent.changeset(
                %FeedEvent{},
                %{ feed: rssfeed.name, event_id: v.id }
              )
            )
          end
        )

        Repo.transaction(m)
    end
  end
end
