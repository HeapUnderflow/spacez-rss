defmodule SpaceZRss.Repo do
  use Ecto.Repo,
    otp_app: :space_z_rss,
    adapter: Ecto.Adapters.Postgres
end
