defmodule SpaceZRss.Worker do
  use GenServer
  require Logger

  alias Nostrum.Api
  alias SpaceZRss.RssApi

  def start_link(ia) do
    iv = %{
      name: ia.name,
      display: ia.display,
      url: ia.url,
      interval: ia.interval,
      post: %{
        channel: ia.post.channel
      }
    }

    GenServer.start_link(__MODULE__, iv)
  end

  def init(ia) do
    setup_refetch(ia)
    {:ok, ia}
  end

  def setup_refetch(%{interval: i} = _state) do
    i = i * 60 * 1000 # min to ms

    Logger.debug("fetching new events in #{i}ms")
    Process.send_after(self(), :feed, i)
  end

  def handle_info(:feed, state) do
    Logger.info("fetching #{state.display} (#{state.url})")

    RssApi.fetch_feed(state.name, state.url)
    |> RssApi.filter_feed()
    |> post_news(state)
    |> RssApi.update_feed()

    # Delay *after* updating the feed, to have a pull on startup.
    setup_refetch(state)

    {:noreply, state}
  end

  defp post_news(rss, %{ post: %{ channel: channel }} = state) do
    Enum.each(
      Enum.reverse(rss.feed.entries),
      fn f ->
        Logger.info("posting new event: #{f.id}", [channel: channel, feedid: rss.name])
        Api.create_message!(channel, format_event_str(f, state))
      end
    )

    rss
  end

  defp format_event_str(entry, state) do
    "#{state.display}: [#{entry.author}] #{entry.title}\n#{entry.link}"
  end
end
