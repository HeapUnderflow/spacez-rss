defmodule SpaceZRss.Init do
  use Task, restart: :transient

  require Logger

  def start_link(ia) do
    Task.start_link(__MODULE__, :run, [ia])
  end

  def run(_ia) do
    conf_loc = Application.get_env(:space_z_rss, :feed_info)
    feed_info = Toml.decode_file!(conf_loc, keys: :atoms)

    Enum.each(feed_info.feeds, fn {name, data} ->
      Map.put(data, :name, to_string(name))
      |> SpaceZRss.Watcher.add_feed()
    end)
  end
end
