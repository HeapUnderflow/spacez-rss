defmodule SpaceZRss.MixProject do
  use Mix.Project

  def project do
    [
      app: :space_z_rss,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: releases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :xmerl],
      mod: {SpaceZRss.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:finch, "~> 0.16.0"},
      {:jason, "~> 1.4"},
      {:toml, "~> 0.7.0"},
      {:feeder_ex, "~> 1.1"},
      {:ecto_sql, "~> 3.10"},
      {:postgrex, "~> 0.17.3"},
      {:nostrum, github: "Kraigie/nostrum", override: true},
      {:aloy_utils, git: "https://gitlab.com/ip-over-avian-carrier/aloy_utils_ex.git"}
    ]
  end

  defp releases do
    [
      spacez_rss: [
        version: "0.1.0",
      ]
    ]
  end

  defp aliases do
    [
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "ecto.setup": ["ecto.create", "ecto.migrate"],
    ]
  end
end
