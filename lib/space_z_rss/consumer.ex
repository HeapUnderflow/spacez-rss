defmodule SpaceZRss.Consumer do
  use Nostrum.Consumer

  require Logger

  @spec handle_event(Nostrum.Consumer.event()) :: any()
  def handle_event({:READY, ready, _wsstate}) do
    Logger.info("logged in as #{ready.user.username} (#{ready.user.id})")
  end
end
